﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ConsoleApp
{
    class Program
    {
        static void Main(string[] args)
        {

            Console.WriteLine("Hello World!");

            char karakter = 'a';

            string szöveg = "Sziasztok"; // referencia típus
            var másikSzöveg = "Helló";

            byte nagyonKisSzám = 1;

            short kisSzám = 6;

            int szám = 8;
            int nagySzám = int.MaxValue;
            uint nagyonNagySzám = uint.MaxValue;

            long nagyonNagyonNagySzám = 2;

            float pí = 3.14f;

            double píDouble = 3.14;

            bool igazHamis = true;


            var tizenkettő = int.Parse("12");
            tizenkettő = System.Int32.Parse("12");

            var különbség = string.Compare(szöveg, másikSzöveg);

            // forrás: https://www.lipsum.com/
            var loremIpsum = "Lorem Ipsum is simply dummy text of the printing and typesetting industry";
            loremIpsum = loremIpsum.Trim();
            Console.WriteLine(loremIpsum.Length + "|" + loremIpsum.ToUpper());
            Console.WriteLine(loremIpsum.Substring(4, 14));

            var sb = new StringBuilder("Lorem Ipsum is simply dummy");
            sb.Append(" text of the printing and typesetting industry");
            Console.WriteLine(sb.ToString());

            var dátum = DateTime.Now;
            Console.WriteLine(string.Format("az idő: {0:T}", dátum));

            var szín = színek.piros;

            if (szín != színek.kék)
            {
                Console.WriteLine(string.Format("A választott szín nem kék, hanem {0} == {0:d}", szín));
            }
            else
            {
                Console.WriteLine("A választott szín kék");
            }

            var újSzín = szín == színek.kék ? színek.piros : színek.zöld;

            switch (újSzín)
            {
                case színek.kék:
                    Console.WriteLine("A választott új szín a kék");
                    break;
                case színek.zöld:
                    Console.WriteLine("A választott új szín a zöld");
                    break;
                default:
                    Console.WriteLine("A választott új szín csak a piros lehet");
                    break;
            }

            if ((szín == színek.kék || szín == színek.zöld) || (újSzín != színek.piros))
            {
                Console.WriteLine("Valamelyik szín nem piros");
            }


            int[,] mátrix = new int[5, 7];
            for (int i = 0; i < mátrix.GetLength(0); i++)
            {
                string sor = "";
                for (int j = 0; j < mátrix.GetLength(1); j++)
                {
                    sor += mátrix[i, j] + " ";
                }
                Console.WriteLine(sor);
            }

            var lista = new List<int>(new[] { 1, 2, 3, 4, 5 });
            Console.WriteLine(string.Format("A lista hossza: {0}, első eleme: {1}", lista.Count, lista[0]));
            foreach (var elem in lista)
            {
                Console.WriteLine(elem);
            }

            var számláló = 12;
            do
            {
                Console.WriteLine("Szalad ez a ciklus!");
                break;
            } while (számláló < 2);

            while (számláló > 2)
            {
                számláló--;
                if (számláló % 2 != 0)
                    continue;

                Console.WriteLine(string.Format("A számláló ( {0} ) páros!", számláló));
            };

            // Osztályok és öröklődés

            ős első = new ős();
            utód második = new utód();
            utód2 harmadik = new utód2();

            ős elsőParam = new ős("szia");
            utód másodikParam = new utód("szia");
            utód2 harmadikParam = new utód2("szia");

            ős o = második;

            Console.WriteLine(második.öt() + " " + második.tíz());
            Console.WriteLine(o.öt() + " " + o.tíz());

            // Könyvtár használata

            Konyvtar.Konyv it = new Konyvtar.Konyv();
            it.Cím = "AZ";
            it.Szerző = "Stephen King";
            it.MegjelenésÉve = 1986;
            //it.ISBN = "12";
            it.ISBN = "9780340364772";

            Console.WriteLine(it);

            Console.WriteLine(Konyvtar.Konyv.ÉrvényesISBN("12"));

            // Referencia paraméterek
            int a = 5;
            int b = 12;
            Csere(ref a, ref b);
            Console.WriteLine("Csere után: a=" + a + " b=" + b);

            int[] tömb = new int[5];
            Console.WriteLine("Tömb hossz=" + tömb.Length);
            Növel(ref tömb, 12);
            Console.WriteLine("Tömb növelés után a hossz=" + tömb.Length);

            Console.WriteLine("Összeadás példa: " + ÖsszeadVégrehajtó((a, b) => { return a + b; }, 1, 5));


            Konyvtar.Konyvespolc polc = new Konyvtar.Konyvespolc(1);
            polc.TelevanaPolc += Polc_Televan;
            polc.Feltesz(it, 0);

        }

        static void Csere(ref int a, ref int b)
        {
            int c = a;
            a = b;
            b = c;
        }

        static void Növel(ref int[] tömb, int hossz)
        {
            tömb = new int[hossz];
        }

        enum színek { kék, zöld, piros };


        delegate int összeadFv(int x, int y);

        static int ÖsszeadVégrehajtó(összeadFv f, int x, int y)
        {
            return f(x, y);
        }

        private static void Polc_Televan(object sender, bool megtelt)
        {
            if (megtelt)
                Console.WriteLine("Polc megtelt");
        }

    }

    class ős
    {
        public ős()
        {
            Console.WriteLine("ős");
        }

        public ős(string szia)
        {
            Console.WriteLine("ős " + szia);
        }

        public int öt()
        {
            return 5;
        }

        public virtual int tíz()
        {
            return 10;
        }
    }

    class utód : ős
    {
        public utód()
        {
            Console.WriteLine("utód");
        }

        public utód(string szia)
        {
            Console.WriteLine("utód " + szia);
        }

        public new int öt()
        {
            return 6;
        }

        public override sealed int tíz()
        {
            return 11;
        }
    }

    class utód2 : utód
    {
        public utód2()
        {
            Console.WriteLine("utód2");
        }
        public utód2(string szia) : base(szia)
        {
            Console.WriteLine("utód2 " + szia);
        }

        public new int öt()
        {
            return 6;
        }

        //public override int tíz()
        //{
        //    return 11;
        //}
    }


}
