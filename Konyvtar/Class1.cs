﻿using System;
using System.Text;

namespace Konyvtar
{
    public class Konyv
    {
        public string Szerző { get; set; }
        public string Cím { get; set; }
        public int MegjelenésÉve { get; set; }

        private string isbn;

        public string ISBN
        {
            get { return isbn; }
            set
            {
                if (Konyv.ÉrvényesISBN(value))
                    isbn = value;
                else
                    throw new Exception("ISBN szám 10 vagy 13 hosszú kell hogy legyen");
            }
        }

        public override string ToString()
        {
            StringBuilder sb = new StringBuilder(Szerző);
            sb.Append(" - ");
            sb.Append(Cím);
            sb.Append(" - ");
            sb.Append(MegjelenésÉve);
            sb.Append(" - ");
            sb.Append(ISBN);

            return sb.ToString();
        }

        public static bool ÉrvényesISBN(string isbn)
        {
            return isbn.Length == 10 || isbn.Length == 13;
        }

    }

    public class Konyvespolc
    {
        Konyv[] konyvek;

        public Konyvespolc(int hossz)
        {
            konyvek = new Konyv[hossz];
        }

        public bool Megtelt
        {
            get
            {
                int darab = 0;
                for (int i = 0; i < konyvek.Length; i++)
                {
                    if (konyvek[i] != null)
                        darab++;
                }
                return darab == konyvek.Length;
            }
        }

        public void Feltesz(Konyv könyv, int pozíció)
        {
            if (konyvek[pozíció] != null)
                throw new Exception("a pozíció foglalt");
            konyvek[pozíció] = könyv;


            if (this.Megtelt && TelevanaPolc != null)
                TelevanaPolc(this, true);
            //if (Megtelt)
            //    TelevanaPolc?.Invoke(this, true);
        }

        public Konyv Levesz(int pozíció)
        {
            if (konyvek[pozíció] == null)
                throw new Exception("a pozíció üres");

            Konyv könyv = konyvek[pozíció];
            konyvek[pozíció] = null;

            return könyv;
        }

        public Konyv this[int pozíció]
        {
            set
            {
                Feltesz(value, pozíció);
            }

            get
            {
                return Levesz(pozíció);
            }
        }

        public delegate void VáltozásFv(object sender, bool megtelt);
        public event VáltozásFv TelevanaPolc;
    }
}
