# C# gyakorlat 2022-23-01

Illés Zoltán  
e-mail: ilzo@inf.elte.hu

[Slide link](https://1drv.ms/p/s!AgZ3ALbexZfctYYmcXYbqgrEEXi9Dg)

2. gyakorlat: c# alapok    
3. gyakorlat: osztály, könyvtár  
4. gyakorlat: események, unit test  
5. gyakorlat: UWP alapok  
6. gyakorlat: Navigáció  
7. gyakorlat: Binding  
8. gyakorlat: SplitView, UserControl, VisualState  
9. gyakorlat: Converter, ItemTemplate, HttpClient, JsonConvert, LocalSettings  

---

Érdekesség:  
https://blogs.windows.com/windowsdeveloper/2022/02/11/window-app-sdk-ecosystem-update/  
https://devblogs.microsoft.com/dotnet/announcing-net-6/  
https://devblogs.microsoft.com/dotnet/the-future-of-net-standard/  
https://blogs.msdn.microsoft.com/ericlippert/2010/02/04/how-many-passes/  
https://blogs.msdn.microsoft.com/ericlippert/2009/06/11/what-does-the-optimize-switch-do/  
