﻿using System;

namespace Sakk_Console
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Sakk játék!");
            JátékMenü();
        }

        static void JátékMenü()
        {
            while (true)
            {
                Console.Clear();
                Console.WriteLine("Választható opciók:");
                Console.WriteLine("  u - új játék");
                Console.WriteLine("  k - kilépés");
                string lépés = Console.ReadLine();
                switch (lépés)
                {
                    case "u":
                        Játék();
                        break;
                    case "k":
                        return;
                    default:
                        continue;

                }
            }
        }

        static void Játék()
        {
            Sakk_Motor.SakkJáték sj = new Sakk_Motor.SakkJáték();
            while (true)
            {
                Console.Clear();

                for (int i = 0; i < 8; i++)
                {
                    string sorString = "";
                    for (int j = 0; j < 8; j++)
                    {
                        if (i == sj.LóSor && j == (int)sj.LóOszlop)
                            sorString += "L";
                        else if (i == sj.GyalogSor && j == (int)sj.GyalogOszlop)
                            sorString += "G";
                        else
                            sorString += "0";
                    }
                    Console.WriteLine(sorString);
                }

                Console.WriteLine("Választható opciók:");
                Console.WriteLine("  l - új lépés");
                Console.WriteLine("  k - kilépés a menübe");
                string lépés = Console.ReadLine();
                switch (lépés)
                {
                    case "l":
                        Console.WriteLine(" -> Sor index:");
                        string sor = Console.ReadLine();
                        int x = int.Parse(sor);
                        Console.WriteLine(" -> Oszlop index:");
                        string oszlop = Console.ReadLine();
                        var y = (Sakk_Motor.SakkJáték.TáblaOszlop)int.Parse(oszlop);

                        sj.Lépés(x, y);

                        break;
                    case "k":
                        return;
                    default:
                        continue;
                }
            }
        }
    }
}
