﻿using System;
using System.ComponentModel;
using System.Text;

namespace Sakk_Motor
{
    public class SakkJáték : INotifyPropertyChanged
    {
        public enum TáblaOszlop { A, B, C, D, E, F, G, H };
        int lóSor = 3;
        TáblaOszlop lóOszlop = TáblaOszlop.G;
        int gyalogSor = 2;
        TáblaOszlop gyalogOszlop = TáblaOszlop.B;

        public SakkJáték()
        { }
        public SakkJáték(int lóSor, TáblaOszlop lóOszlop, int gyalogSor, TáblaOszlop gyalogOszlop)
        {
            this.lóSor = lóSor;
            this.lóOszlop = lóOszlop;
            this.gyalogSor = gyalogSor;
            this.gyalogOszlop = gyalogOszlop;
        }

        public int LóSor { get => lóSor; }
        public TáblaOszlop LóOszlop { get => lóOszlop; }
        public int GyalogSor { get => gyalogSor; }
        public TáblaOszlop GyalogOszlop { get => gyalogOszlop; }

        private int lépésSzám;

        public int LépésSzám
        {
            get { return lépésSzám; }
            private set
            {
                if (value == lépésSzám + 1)
                {
                    lépésSzám = value;
                    PropertyChanged?.Invoke(this,
                    new PropertyChangedEventArgs(nameof(LépésSzám)));
                }
                else
                {
                    throw new Exception("A lépésszámot csak 1el növelni lehet");
                }
            }
        }


        public override string ToString()
        {
            StringBuilder sb = new StringBuilder("Sakk játék");
            sb.Append(" - Ló pozíciója: ");
            sb.Append(lóOszlop);
            sb.Append(lóSor);
            return sb.ToString();
        }


        public void Lépés(int sor, TáblaOszlop oszlop)
        {
            int x = sor;
            int y = (int)oszlop;

            if (!ÉrvényesLépés(lóSor, (int)lóOszlop, x, y))
            {
                throw new Exception("Érvénytelen lépés!");
            }

            lóSor = sor;
            lóOszlop = oszlop;
            LépésSzám++;

            if (JátékVége())
                játékVége?.Invoke();
        }

        public bool ÉrvényesLépés(int aktuálisSor, int aktuálisOszlop, int sor, int oszlop)
        {
            return (aktuálisSor < 8 && sor < 8 && aktuálisSor >= 0 && sor >= 0 &&
                                   aktuálisOszlop < 8 && oszlop < 8 && aktuálisOszlop >= 0 && oszlop >= 0) &&
                                ((Math.Abs(aktuálisSor - sor) == 1 && Math.Abs(aktuálisOszlop - oszlop) == 2)
                                || (Math.Abs(aktuálisSor - sor) == 2 && Math.Abs(aktuálisOszlop - oszlop) == 1));
        }

        public bool JátékVége()
        {
            return lóSor == gyalogSor && lóOszlop == gyalogOszlop;
        }

        public delegate void JátékVégeFn();
        public event JátékVégeFn játékVége;
        public event PropertyChangedEventHandler PropertyChanged;
    }

}
