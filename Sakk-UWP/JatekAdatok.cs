﻿using System.ComponentModel;

namespace Sakk_UWP
{
    class JatekAdatok : INotifyPropertyChanged
    {

        private static JatekAdatok példány = null;

        private string játékosNév;

        public event PropertyChangedEventHandler PropertyChanged;

        public string JátékosNév
        {
            get { return játékosNév; }
            set
            {
                játékosNév = value;
                PropertyChanged?.Invoke(this,
                    new PropertyChangedEventArgs(nameof(JátékosNév)));
            }
        }

        public static JatekAdatok Példány
        {
            get
            {
                if (példány == null)
                    példány = new JatekAdatok();
                return példány;
            }
        }

        private JatekAdatok()
        {
            JátékosNév = "Név";
        }
    }
}
