﻿using System;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Media;
using Sakk_Motor;

// The Blank Page item template is documented at https://go.microsoft.com/fwlink/?LinkId=402352&clcid=0x409

namespace Sakk_UWP
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class MainPage : Page
    {
        SakkMezo aktuális;
        SakkJáték sj;

        public MainPage()
        {
            this.InitializeComponent();
            JatekAdatok ja = JatekAdatok.Példány;
            this.DataContext = ja;
            pályaLétrehozása();
        }

        void pályaLétrehozása()
        {
            sj = new SakkJáték();
            lépésText.DataContext = sj;

            sj.játékVége += Sj_játékVége;
            pályaGrid.Children.Clear();
            pályaGrid.ColumnDefinitions.Clear();
            pályaGrid.RowDefinitions.Clear();

            aktuális = null;

            RowDefinition rd;
            ColumnDefinition cd;

            cd = new ColumnDefinition();
            cd.Width = new GridLength(1, GridUnitType.Star);
            pályaGrid.ColumnDefinitions.Add(cd);
            rd = new RowDefinition();
            rd.Height = new GridLength(1, GridUnitType.Star);
            pályaGrid.RowDefinitions.Add(rd);

            for (int i = 0; i < 8; i++)
            {
                cd = new ColumnDefinition();
                cd.Width = new GridLength(1, GridUnitType.Auto);
                pályaGrid.ColumnDefinitions.Add(cd);
                rd = new RowDefinition();
                rd.Height = new GridLength(1, GridUnitType.Auto);
                pályaGrid.RowDefinitions.Add(rd);
                for (int j = 0; j < 8; j++)
                {
                    SakkMezo b = new SakkMezo();
                    b.MinHeight = 50;
                    b.MaxHeight = 50;
                    b.MinWidth = 50;
                    b.MaxWidth = 50;
                    b.Margin = new Thickness(1);
                    b.Tapped += B_Click;
                    b.Background = new SolidColorBrush((i + j) % 2 == 0 ? Windows.UI.Colors.Brown : Windows.UI.Colors.Gray);
                    Grid.SetColumn(b, i + 1);
                    Grid.SetRow(b, j + 1);
                    pályaGrid.Children.Add(b);

                    if (i == sj.LóSor && j == (int)sj.LóOszlop)
                    {
                        aktuális = b;
                        aktuális.Ló = true;
                    }
                    if (i == sj.GyalogSor && j == (int)sj.GyalogOszlop)
                    {
                        b.Gyalog = true;
                    }
                }
            }

            cd = new ColumnDefinition();
            cd.Width = new GridLength(1, GridUnitType.Star);
            pályaGrid.ColumnDefinitions.Add(cd);
            rd = new RowDefinition();
            rd.Height = new GridLength(1, GridUnitType.Star);
            pályaGrid.RowDefinitions.Add(rd);

        }

        private async void Sj_játékVége()
        {
            ContentDialog cd = new ContentDialog();
            cd.Content = "Játék véget ért";
            cd.CloseButtonText = "OK";
            await cd.ShowAsync();
        }

        private async void B_Click(object sender, RoutedEventArgs e)
        {
            SakkMezo én = sender as SakkMezo;
            int aktuálisX = Grid.GetColumn(aktuális) - 1;
            int aktuálisY = Grid.GetRow(aktuális) - 1;
            int x = Grid.GetColumn(én) - 1;
            int y = Grid.GetRow(én) - 1;

            try
            {
                sj.Lépés(x, (SakkJáték.TáblaOszlop)y);
                aktuális.Ló = false;
                aktuális = én;
                aktuális.Ló = true;
            }
            catch (Exception)
            {
                ContentDialog cd = new ContentDialog();
                cd.Content = "érvénytelen lépés";
                cd.CloseButtonText = "OK";
                await cd.ShowAsync();
            }
        }

        private bool Vissza()
        {
            if (Frame.CanGoBack)
            {
                Frame.GoBack();
                return true;
            }
            return false;
        }

        private void GoBackBtn_Click(object sender, RoutedEventArgs e)
        {
            Vissza();
        }

    }
}
