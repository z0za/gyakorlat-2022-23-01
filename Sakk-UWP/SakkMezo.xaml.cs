﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// The User Control item template is documented at https://go.microsoft.com/fwlink/?LinkId=234236

namespace Sakk_UWP
{
    public sealed partial class SakkMezo : UserControl
    {
        public SakkMezo()
        {
            this.InitializeComponent();
        }

        public bool Ló
        {
            get { return (bool)GetValue(LóProp); }
            set
            {
                SetValue(LóProp, value);

            }
        }

        public static readonly DependencyProperty LóProp =
            DependencyProperty.Register(nameof(Ló), typeof(bool),
                typeof(SakkMezo), null);

        public bool Gyalog
        {
            get { return (bool)GetValue(GyalogProp); }
            set
            {
                SetValue(GyalogProp, value);

            }
        }

        public static readonly DependencyProperty GyalogProp =
            DependencyProperty.Register(nameof(Gyalog), typeof(bool),
                typeof(SakkMezo), null);

    }
}
