﻿namespace TestProject1
{
    public class KonyvTests
    {
        struct Teszt { public string bemenet; public bool kimenet; };

        [SetUp]
        public void Setup()
        {
        }

        [Test]
        public void ÉrvényesISBNTest()
        {
            Teszt[] tesztek = new Teszt[]{
                new Teszt(){bemenet="1234567890",kimenet=true },
                new Teszt(){bemenet="1234567890123",kimenet=true },
                //new Teszt(){bemenet="alma",kimenet=true },
                new Teszt(){bemenet="alma",kimenet=false },
            };
            foreach (var teszt in tesztek)
            {
                Assert.AreEqual(teszt.kimenet, Konyvtar.Konyv.ÉrvényesISBN(teszt.bemenet));
            }
        }
    }

    public class KonyvespolcTests
    {
        [SetUp]
        public void Setup()
        {
        }

        [Test]
        public void MegteltTest()
        {
            Konyvtar.Konyv it = new Konyvtar.Konyv();
            it.Cím = "AZ";
            it.Szerző = "Stephen King";
            it.MegjelenésÉve = 1986;
            it.ISBN = "9780340364772";

            Konyvtar.Konyvespolc polc = new Konyvtar.Konyvespolc(1);
            polc.Feltesz(it, 0);

            Assert.AreEqual(true, polc.Megtelt);
        }
    }
}