﻿using System.ComponentModel;

namespace UWPApp1
{

    public class Adatok : INotifyPropertyChanged
    {
        private static Adatok példány = null;

        private int valamiSzám;

        public event PropertyChangedEventHandler PropertyChanged;

        public int ValamiSzám
        {
            get { return valamiSzám; }
            set
            {
                valamiSzám = value;
                PropertyChanged?.Invoke(this,
                    new PropertyChangedEventArgs(nameof(ValamiSzám)));
            }
        }

        public static Adatok Példány
        {
            get
            {
                if (példány == null)
                    példány = new Adatok();
                return példány;
            }
        }

        private Adatok()
        {
            valamiSzám = 5;
        }

    }
}
