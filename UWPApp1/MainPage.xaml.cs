﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Core;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// The Blank Page item template is documented at https://go.microsoft.com/fwlink/?LinkId=402352&clcid=0x409

namespace UWPApp1
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class MainPage : Page
    {
        public MainPage()
        {
            this.InitializeComponent();
            BButton.Click += BButton_Click;

            var view = Windows.UI.Core.SystemNavigationManager.GetForCurrentView();
            view.AppViewBackButtonVisibility =
                AppViewBackButtonVisibility.Visible;
            view.BackRequested += View_BackRequested;

            KeyboardAccelerator vissza = new KeyboardAccelerator();
            vissza.Key = Windows.System.VirtualKey.GoBack;
            vissza.Invoked += Vissza_Invoked;

            KeyboardAccelerator altvissza = new KeyboardAccelerator();
            altvissza.Key = Windows.System.VirtualKey.Left;
            altvissza.Invoked += Vissza_Invoked;
            altvissza.Modifiers = Windows.System.VirtualKeyModifiers.Menu;
            KeyboardAccelerators.Add(vissza);
            KeyboardAccelerators.Add(altvissza);
        }

        private void BButton_Click(object sender, RoutedEventArgs e)
        {

            //((((Window.Current.Content as Frame)
            //    .Content as MainPage)
            //        .Content as Grid)
            //            .Children[2] as Button).Content
            //                = "almafa112";
            ÜzenetAblak("Hello");
        }

        public async void ÜzenetAblak(string üzenet)
        {

            var rand = new Random();
            ContentDialog cd = new ContentDialog()
            {
                CloseButtonText = "OK",
                Content = üzenet + rand.Next(5, 19),
                Title = "Szia"
            };
            await cd.ShowAsync();

            Button b = new Button();
            b.Content = "új";
            Grid.SetColumn(b, 1);
            grid1.Children.Add(b);



            return;
        }


        private void Almagomb_Click(object sender, RoutedEventArgs e)
        {
            Button én = sender as Button;
            én.Content = "almafa 12";
        }

        private void Vissza_Invoked(KeyboardAccelerator sender, KeyboardAcceleratorInvokedEventArgs args)
        {
            args.Handled = Vissza();
        }

        private void View_BackRequested(object sender, BackRequestedEventArgs e)
        {
            e.Handled = Vissza();
        }

        private bool Vissza()
        {
            if (Frame.CanGoBack)
            {
                Frame.GoBack();
                return true;
            }
            return false;
        }

        private void GoBackBtn_Click(object sender, RoutedEventArgs e)
        {
            Vissza();
        }
    }
}
