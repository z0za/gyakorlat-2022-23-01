﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.Storage;
using Windows.System;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// The Blank Page item template is documented at https://go.microsoft.com/fwlink/?LinkId=234238

namespace UWPApp1
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class MenuPage : Page
    {

        DispatcherTimer timer;
        public MenuPage()
        {
            this.InitializeComponent();
            this.NavigationCacheMode = NavigationCacheMode.Enabled;

            timer = new DispatcherTimer();
            timer.Interval = new TimeSpan(0, 0, 1);
            timer.Tick += Timer_Tick;
            timer.Start();
        }
        private void Timer_Tick(object sender, object e)
        {
            Time += new TimeSpan(0, 0, 10);
        }

        public static readonly DependencyProperty TimeProperty =
                DependencyProperty.Register(nameof(Time), typeof(TimeSpan),
                typeof(MenuPage), new PropertyMetadata(new TimeSpan(0, 0, 50)));
        public TimeSpan Time
        {
            get
            {
                return (TimeSpan)GetValue(TimeProperty);
            }
            set
            {
                SetValue(TimeProperty, value);
            }
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            Frame.Navigate(typeof(MainPage));
        }

        private void Button_Click_1(object sender, RoutedEventArgs e)
        {
            Frame.Navigate(typeof(AdatokPage));

        }

        private async void SplitPaneButton_Tapped(object sender, TappedRoutedEventArgs e)
        {
            HttpClient hc = new HttpClient();
            var válasz = await hc.GetAsync("https://jsonplaceholder.typicode.com/users");
            string válaszSzöveg = await válasz.Content.ReadAsStringAsync();

            User[] users =
                Newtonsoft.Json.JsonConvert.DeserializeObject<User[]>(válaszSzöveg);
            listbox1.ItemsSource = users;
        }

        private void Savebtn_Click(object sender, RoutedEventArgs e)
        {
            ApplicationDataContainer ls = ApplicationData.Current.LocalSettings;
            ls.Values["üzenet"] = "sziasztok";
        }

        private async void Loadbtn_Click(object sender, RoutedEventArgs e)
        {
            ApplicationDataContainer ls = ApplicationData.Current.LocalSettings;
            if (ls.Values.ContainsKey("üzenet"))
            {
                string üzenet = ls.Values["üzenet"].ToString();
                ContentDialog cd = new ContentDialog()
                {
                    CloseButtonText = "OK",
                    Content = üzenet,
                    Title = "Szia"
                };
                await cd.ShowAsync();
            }
        }

    }
}
