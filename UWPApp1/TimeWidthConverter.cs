﻿using System;
using Windows.UI.Xaml.Data;

namespace UWPApp1
{
    public class TimeWidthConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, string language)
        {
            return ((TimeSpan)value).TotalSeconds;
        }

        public object ConvertBack(object value, Type targetType, object parameter, string language)
        {
            return new TimeSpan(0, 0, (int)value);
        }
    }
}
